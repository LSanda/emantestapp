//
//  CurrencyTests.swift
//  eManCartAppTests
//
//  Created by Lukáš Šanda on 19.09.18.
//  Copyright © 2018 Lukáš Šanda. All rights reserved.
//

import XCTest
@testable import eManCartApp


class CurrencyTests: XCTestCase {
    let context = CoreDataManager.shared.viewContext
    
    override func setUp() {
        super.setUp()
        
        ProductsManager.shared.initProductsForCart()
        UserDefaults.standard.set(0.85666, forKey: "eManCartApp.Currency.USDEUR")
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testChangeCurrencyForItem() {
        if let milk = ProductsManager.shared.getProduct("Milk") {
            let ratio = UserDefaults.standard.double(forKey: "eManCartApp.Currency.USDEUR")
            milk.price = milk.price * ratio
            XCTAssert(milk.price < DefaultPrice.milk)
        }
    }
    
    func testExchangeRate() {
        let promise = expectation(description: "Exchange ratio has not been calculated.")
        
        CurrencyManager.shared.exchangeRatio(for: "EUR") { (error) in
            XCTAssertNil(error, "Error is not null")
            
            let ratio = UserDefaults.standard.double(forKey: "eManCartApp.Currency.USDEUR")
            let milkPriceEUR = DefaultPrice.milk * ratio
            XCTAssert(milkPriceEUR < DefaultPrice.milk)
            promise.fulfill()
        }
        
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func testGetAvailableCurrencies() {
        let promise = expectation(description: "The response did not contain the required data.")
        
        CurrencyManager.shared.getCurrencies { (_, error) in
            XCTAssertNil(error)
            if let currencies = UserDefaults.standard.availableCurrencies {
                XCTAssert(currencies.count > 0)
                XCTAssert(currencies.contains { item in
                    return item.key == "CZK"
                })
                
                promise.fulfill()
            }
        }
        waitForExpectations(timeout: 5, handler: nil)
    }
    
}
