//
//  ProductsTests.swift
//  eManCartAppTests
//
//  Created by Lukáš Šanda on 21.09.18.
//  Copyright © 2018 Lukáš Šanda. All rights reserved.
//

import XCTest
@testable import eManCartApp


class ProductsTests: XCTestCase {
    let context = CoreDataManager.shared.viewContext
    
    override func setUp() {
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testAmountInCart() {
        let cart = Cart(context: context)
        
        if let milk = ProductsManager.shared.getProduct("Milk"), let eggsFirst = ProductsManager.shared.getProduct("Eggs"), let eggsSecond = ProductsManager.shared.getProduct("Eggs") {
            cart.addToCart(milk)
            cart.addToCart(eggsFirst)
            cart.addToCart(eggsSecond)
            
            XCTAssert(cart.items?.count == 3)
            XCTAssert(CartManager.shared.amountOfProduct(for: "Eggs") == 2)
            XCTAssert(CartManager.shared.amountOfProduct(for: "Beans") == 0)
            XCTAssert(CartManager.shared.amountOfProduct(for: "Milk") == 1)
        }
    }
    
    func testReadProductsFromJSON() {
        let products = ProductsManager.shared.products
        
        XCTAssert(products.count == 4)
    }
    
}
