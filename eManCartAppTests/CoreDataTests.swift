//
//  CoreDataTests.swift
//  CoreDataTests
//
//  Created by Lukáš Šanda on 18.09.18.
//  Copyright © 2018 Lukáš Šanda. All rights reserved.
//

import XCTest
@testable import eManCartApp


class CoreDataTests: XCTestCase {
    let context = CoreDataManager.shared.viewContext
    
    override func setUp() {
        super.setUp()
        
        ProductsManager.shared.initProductsForCart()
        UserDefaults.standard.set(0.85666, forKey: "eManCartApp.Currency.USDEUR")
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testRemoveItemsFromCart() {
        let milk = Item(context: context)
        let eggs = Item(context: context)
        
        let cart = Cart(context: context)
        cart.addToItems(milk)
        cart.addToItems(eggs)
        
        if let items = cart.items {
            XCTAssert(items.count == 2)
            XCTAssert(items.contains(milk))
            
            cart.removeFromItems(milk)
            
            XCTAssert(items.contains(eggs))
            XCTAssert(!items.contains(milk))
        }
    }
    func testAddItemToCart() {
        let milk = Item(context: context)
        let eggs = Item(context: context)
        
        let cart = Cart(context: context)
        cart.addToItems(milk)
        cart.addToItems(eggs)
        
        if let items = cart.items {
            XCTAssert(items.count == 2)
            XCTAssert(items.contains(milk))
        }
    }
    
    func testRemoveItemFromCart() {
        let prod = ProductsManager.shared.products[0]
        let prod2 = ProductsManager.shared.products[1]
        
        let cart = Cart(context: context)
        cart.addToItems(prod)
        cart.addToItems(prod2)
        CoreDataManager.shared.saveContext()
        
        cart.removeProductFromCart(prod.name)
        
        if let items = cart.items {
            XCTAssert(items.count == 1)
            XCTAssertFalse(items.contains(prod))
        }
    }
}
