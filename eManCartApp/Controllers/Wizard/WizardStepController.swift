//
//  WizardStepController.swift
//  eManCartApp
//
//  Created by Lukáš Šanda on 20.09.18.
//  Copyright © 2018 Lukáš Šanda. All rights reserved.
//

import UIKit

class WizardStepController: UIViewController {
    
    // MARK: - Properties
    
    var titleText: String!
    var subtitleText: String!
    var image: UIImage!
    
    var stepIndex: Int!
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text = titleText
        subtitleLabel.text = subtitleText
        imageView.image = image
    }
    
}
