//
//  WizardPageController.swift
//  eManCartApp
//
//  Created by Lukáš Šanda on 20.09.18.
//  Copyright © 2018 Lukáš Šanda. All rights reserved.
//

import UIKit
import SnapKit

struct WizardStep {
    let title: String
    let subtitle: String
    let image: UIImage?
}

class WizardPageController: UIPageViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    // MARK: - Properties
    
    var pageControl: UIPageControl!
    var btnBack: UIButton!
    var btnNext: UIButton!
    
    let wizardSteps = [
        WizardStep(
            title: "WIZARD_WELCOME_TITLE".localized,
            subtitle: "WIZARD_WELCOME_SUBTITLE".localized,
            image: #imageLiteral(resourceName: "Welcome")),
        WizardStep(
            title: "WIZARD_CART_TITLE".localized,
            subtitle: "WIZARD_CART_SUBTITLE".localized,
            image: #imageLiteral(resourceName: "Cart")),
        WizardStep(
            title: "WIZARD_CURRENCIES_TITLE".localized,
            subtitle: "WIZARD_CURRENCIES_SUBTITLE".localized,
            image: #imageLiteral(resourceName: "Currencies"))
    ]
    
 
    // MARK: - Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if wizardSteps.count > 0 {
            let firstController = getStepController(0)!
            let startingControllers = [firstController]
            self.setViewControllers(startingControllers, direction: .forward, animated: false, completion: nil)
        }
        
        self.dataSource = self
        self.delegate = self
        
        initComponents()
    }
    
    // MARK: UIPageControl delegate methods
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if !completed { return }
        if let currentViewController = self.viewControllers?.last as? WizardStepController {
            let currentIndex = currentViewController.stepIndex
            
            self.pageControl.currentPage = currentIndex!
        }
    }
    
    // MARK: UIPageControl data source methods
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let stepController = viewController as! WizardStepController
        
        if stepController.stepIndex > 0 {
            return getStepController(stepController.stepIndex - 1)
        }
        
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let stepController = viewController as! WizardStepController
        
        if stepController.stepIndex + 1 < wizardSteps.count {
            return getStepController(stepController.stepIndex + 1)
        }
        
        return nil
    }
    
    // MARK: - Private Methods
    
    /// Method for configuration Page Control and Button components
    private func initComponents() {
        pageControl = UIPageControl()
        pageControl.numberOfPages = wizardSteps.count
        pageControl.currentPage = 0
        pageControl.pageIndicatorTintColor = UIColor.gray
        pageControl.currentPageIndicatorTintColor = UIColor.darkGray
        
        self.view.addSubview(pageControl)
        
        pageControl.snp.makeConstraints {
            $0.centerX.equalToSuperview()
        }
        
        btnBack = UIButton(type: .system)
        btnBack.setTitle("WIZARD_BUTTON_BACK".localized, for: UIControl.State())
        btnBack.setTitleColor(UIColor.black, for: UIControl.State())
        btnBack.addTarget(self, action: #selector(buttonBackTouched), for: .touchUpInside)
        
        self.view.addSubview(btnBack)
        
        btnBack.snp.makeConstraints {
            $0.centerY.equalTo(pageControl)
            if #available(iOS 11.0, *) {
                $0.bottom.equalTo(view.safeAreaLayoutGuide).inset(18)
                $0.leading.equalTo(view.safeAreaLayoutGuide).inset(24)
            } else {
                $0.bottom.equalToSuperview().inset(18)
                $0.leading.equalToSuperview().inset(24)
            }
        }
        
        btnNext = UIButton(type: .system)
        btnNext.setTitle("WIZARD_BUTTON_NEXT".localized, for: UIControl.State())
        btnNext.setTitleColor(UIColor.black, for: UIControl.State())
        btnNext.addTarget(self, action: #selector(buttonNextTouched), for: .touchUpInside)
        
        self.view.addSubview(btnNext)
        
        btnNext.snp.makeConstraints {
            $0.centerY.equalTo(pageControl)
            if #available(iOS 11.0, *) {
                $0.bottom.equalTo(view.safeAreaLayoutGuide).inset(18)
                $0.trailing.equalTo(view.safeAreaLayoutGuide).inset(24)
            } else {
                $0.bottom.equalToSuperview().inset(18)
                $0.trailing.equalToSuperview().inset(24)
            }
        }
    }
    
    
    /// Methods which returns next step controller in UIPageControl component
    ///
    /// - Parameter stepIndex: Index of class
    /// - Returns: Controller
    private func getStepController(_ stepIndex: Int) -> WizardStepController? {
        if stepIndex < wizardSteps.count {
            let stepController = storyboard!.instantiateViewController(withIdentifier: "WizardStepController") as? WizardStepController
            
            stepController?.titleText = wizardSteps[stepIndex].title
            stepController?.subtitleText = wizardSteps[stepIndex].subtitle
            stepController?.image = wizardSteps[stepIndex].image
            
            stepController?.stepIndex = stepIndex
            return stepController
        } else {
            return nil
        }
    }
    
    /// Action method for Button "Next"
    ///
    /// - Parameter sender: Sender
    @objc private func buttonNextTouched(sender: UIButton!) {
        
        if self.pageControl.currentPage < wizardSteps.count - 1 {
            if var currentIndex = (viewControllers?.last as? WizardStepController)?.stepIndex {
                currentIndex += 1
                
                if let nextController = getStepController(currentIndex) {
                    self.pageControl.currentPage = currentIndex
                    
                    self.setViewControllers([nextController], direction: .forward, animated: true, completion: nil)
                }
            }
        } else {
            let cartStoryboard: UIStoryboard = .cart
            let viewController = cartStoryboard.instantiateViewController(withIdentifier: "CartNavigationController")
            self.present(viewController, animated: true, completion: {
                UserDefaults.standard.isWizardDisplayed = true
            })
        }
    }
    
    /// Action method for Button "Back"
    ///
    /// - Parameter sender: Sender
    @objc private func buttonBackTouched(sender: UIButton!) {
        if self.pageControl.currentPage > 0 {
            if var currentIndex = (viewControllers?.last as? WizardStepController)?.stepIndex {
                currentIndex -= 1
                
                if let nextController = getStepController(currentIndex) {
                    self.pageControl.currentPage = currentIndex
                    
                    self.setViewControllers([nextController], direction: .reverse, animated: true, completion: nil)
                }
            }
        }
    }
}

