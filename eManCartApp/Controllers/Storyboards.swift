//
//  Storyboards.swift
//  eManCartApp
//
//  Created by Lukáš Šanda on 20.09.18.
//  Copyright © 2018 Lukáš Šanda. All rights reserved.
//

import UIKit


extension UIStoryboard {
    
    static let wizard                   = UIStoryboard(name: "Wizard")
    static let cart                     = UIStoryboard(name: "Cart")
}

extension UIStoryboard {
    
    convenience init(name: String) {
        self.init(name: name, bundle: nil)
    }
    
}
