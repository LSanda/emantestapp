//
//  CartController.swift
//  eManCartApp
//
//  Created by Lukáš Šanda on 21.09.18.
//  Copyright © 2018 Lukáš Šanda. All rights reserved.
//

import UIKit

class CartController: UITableViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    // MARK: - Properties
    
    var cart = CartManager.shared.loadCart()
    var currency = ""
    var currencies = [String]()
    let tableRowHeight: CGFloat = 100
    
    var internetConnectionView = UIView()
    
    // MARK: - Methods
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationItem.rightBarButtonItem?.isEnabled = CartManager.shared.summaryPriceForCart() > 0
        ReachabilityManager.shared.isConnectedToNetwork()
        
        if !ReachabilityManager.shared.isConnected {
            self.showNoConnectionView()
        }
        
        let currency = UserDefaults.standard.currentCurrency
        ProductsManager.shared.changeItemsPrice(for: currency) { error in
            if let _ = error {
                self.showNoConnectionView()
            } else {
                self.tableView.reloadData()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.registerReusableNibCell(for: ProductCell.self)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadViewAfterPay), name: .paySummary, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showNoConnectionView), name: .showNoConnectionView, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(hideNoConnectionView), name: .hideNoConnectionView, object: nil)
        
        if cart == nil {
            self.cart = Cart(context: CoreDataManager.shared.viewContext)
        }
        
        internetConnectionView = InternetIndicator.shared.noInternetView()
        UIApplication.shared.keyWindow?.addSubview(internetConnectionView)
        
        localizeStrings()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ProductsManager.shared.products.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableRowHeight
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> ProductCell {
        let cell: ProductCell = tableView.dequeueReusableCell(for: indexPath)
        cell.configure(index: indexPath.row)
        
        cell.btnPlus.addTarget(self, action: #selector(btnPlusTouched(_:)), for: .touchUpInside)
        cell.btnMinus.addTarget(self, action: #selector(btnMinusTouched(_:)), for: .touchUpInside)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    // MARK: - Picker View Methods
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return currencies.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return currencies[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        currency = currencies[row]
    }
    
    // MARK: - Private Methods
    
    @IBAction private func btnPlusTouched(_ sender: UIButton) {
        self.navigationItem.rightBarButtonItem?.isEnabled = true
        
        if let cart = cart {
            cart.addToCart(ProductsManager.shared.products[sender.tag])
            tableView.reloadData()
        }
    }
    
    @IBAction private func btnMinusTouched(_ sender: UIButton) {
        self.navigationItem.rightBarButtonItem?.isEnabled = CartManager.shared.summaryPriceForCart() > 0
        
        if let cart = cart {
            cart.removeProductFromCart(ProductsManager.shared.products[sender.tag].name)
            tableView.reloadData()
        }
    }
    
    @IBAction private func btnFinishOrderTouched(_ sender: UIBarButtonItem) {
        self.hideNoConnectionView()
        
        let alert = SummaryManager.shared.showAlertWithSummary()
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction private func btnCurrencyTouched(_ sender: UIBarButtonItem) {
        ReachabilityManager.shared.isConnectedToNetwork()
        
        if !ReachabilityManager.shared.isConnected {
            self.showNoConnectionView()
            return
        }
        
        currency = ""
        self.hideNoConnectionView()
        
        currencies = CurrencyManager.shared.availableSortedCurrencies()
        
        let message = "\n\n\n\n\n\n"
        let alert = UIAlertController(title: "CURRENCIES_AVAILABLE_CURRENCIES".localized, message: message, preferredStyle: .actionSheet)
        alert.isModalInPopover = true
        
        let pickerFrame = UIPickerView(frame: CGRect(x: 5, y: 20, width: self.view.bounds.width - 20, height: 140))

        pickerFrame.delegate = self
        pickerFrame.dataSource = self
        
        alert.view.addSubview(pickerFrame)
        let okAction = UIAlertAction(title: "ALERT_OK".localized, style: .default) { _ in
            self.reloadWithNewCurrency()
        }
        alert.addAction(okAction)
        let cancelAction = UIAlertAction(title: "ALERT_CANCEL".localized, style: .destructive, handler: nil)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true, completion: nil)
    }

    
    /// Method which reload whole view after select currency
    private func reloadWithNewCurrency() {
        UIApplication.shared.keyWindow?.showBlurLoader()
        
        if currency.elementsEqual("") {
            currency = currencies[0]
        }
        
        UserDefaults.standard.currentCurrency = CurrencyManager.shared.currencyKey(for: currency)
        let key = UserDefaults.standard.currentCurrency
        ProductsManager.shared.changeItemsPrice(for: key) { error in
            if let _ = error {
                self.showNoConnectionView()
            } else {
                self.tableView.reloadData()
            }
            UIApplication.shared.keyWindow?.removeBlurLoader()
        }
    }
    
    /// Method for hiding "No Connection" view
    @objc private func hideNoConnectionView() {
        self.navigationItem.leftBarButtonItem?.isEnabled = true
        
        UIView.animate(withDuration: 1, animations: {
            self.internetConnectionView.backgroundColor = .white
        }) { _ in
            self.internetConnectionView.isHidden = true
        }
    }
    
    /// Method for showing "No Connection" view
    @objc private func showNoConnectionView() {
        self.navigationItem.leftBarButtonItem?.isEnabled = false
        self.internetConnectionView.isHidden = false
        
        UIView.animate(withDuration: 2, animations: {
            self.internetConnectionView.backgroundColor = .red
        })
    }
    
    /// Method which reload view after paying for products in the cart
    @objc private func reloadViewAfterPay() {
        self.navigationItem.rightBarButtonItem?.isEnabled = false
        cart = Cart(context: CoreDataManager.shared.viewContext)
        self.tableView.reloadData()
    }
    
    /// Method which set localized strings
    private func localizeStrings() {
        if let leftItem = self.navigationItem.leftBarButtonItem,
            let rightItem = self.navigationItem.rightBarButtonItem {
            leftItem.title = "CART_CHANGE_CURRENCY".localized
            rightItem.title = "CART_TO_THE_ORDER".localized
        }
    }
}
