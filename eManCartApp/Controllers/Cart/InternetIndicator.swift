//
//  InternetIndicator.swift
//  eManCartApp
//
//  Created by Lukáš Šanda on 22.09.18.
//  Copyright © 2018 Lukáš Šanda. All rights reserved.
//

import UIKit
import SnapKit


class InternetIndicator {
    
    // MARK: - Properties
    
    static let shared: InternetIndicator = { return InternetIndicator() }()
    
    // MARK: - Methods
    
    /// Function which create View which indicates no connection to the internet
    ///
    /// - Returns: View
    func noInternetView() -> UIView {
        let screenSize: CGRect = UIScreen.main.bounds
        let btnReload = UIButton()
        btnReload.setTitle("NO_CONNECTION_BUTTON_TEXT".localized, for: .normal)
        btnReload.addTarget(self, action: #selector(refreshInternetConnection), for: .touchUpInside)
        
        let view = UIView(frame: CGRect(x: 0, y: screenSize.height - 50, width: screenSize.width, height: 50))
        view.backgroundColor = .white
        
        view.addSubview(btnReload)
        
        btnReload.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.centerY.equalToSuperview()
        }
        
        return view
    }
    
    /// Method which check connection
    @objc func refreshInternetConnection() {
        ReachabilityManager.shared.isConnectedToNetwork()
        
        if ReachabilityManager.shared.isConnected {
            NotificationCenter.default.post(name: .hideNoConnectionView, object: nil)
        }
    }
}
