//
//  ReusableViewType.swift
//  eManCartApp
//
//  Created by Lukáš Šanda on 20.09.18.
//  Copyright © 2018 Lukáš Šanda. All rights reserved.
//

import Foundation
import UIKit

protocol ReusableViewType {
    static var reuseIdentifier: String { get }
}

extension ReusableViewType where Self: UIView {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}

// MARK: - UITableView

extension UITableViewCell: ReusableViewType {}
extension UITableViewHeaderFooterView: ReusableViewType {}

extension UITableView {
    
    func registerReusableNibCell<Cell: UITableViewCell>(for: Cell.Type) {
        let nib = UINib(nibName: Cell.reuseIdentifier, bundle: Bundle(for: Cell.self))
        self.register(nib, forCellReuseIdentifier: Cell.reuseIdentifier)
    }
    
    func registerReusableCell<Cell: UITableViewCell>(for: Cell.Type) {
        self.register(Cell.self, forCellReuseIdentifier: Cell.reuseIdentifier)
    }
    
    func registerReusableHeaderFooterNibView<View: UIView>(for: View.Type) where View: ReusableViewType {
        let nib = UINib(nibName: View.reuseIdentifier, bundle: Bundle(for: View.self))
        self.register(nib, forHeaderFooterViewReuseIdentifier: View.reuseIdentifier)
    }
    
    func registerReusableHeaderFooterView<View: UIView>(for: View.Type) where View: ReusableViewType {
        self.register(View.self, forHeaderFooterViewReuseIdentifier: View.reuseIdentifier)
    }
    
    func dequeueReusableHeaderFooterView<View: UIView>() -> View where View: ReusableViewType {
        return self.dequeueReusableHeaderFooterView(withIdentifier: View.reuseIdentifier) as! View
    }
    
    func dequeueReusableCell<T: UITableViewCell>(for indexPath: IndexPath) -> T {
        return self.dequeueReusableCell(withIdentifier: T.reuseIdentifier, for: indexPath) as! T
    }
    
}

// MARK: - UICollectionView

extension UICollectionViewCell: ReusableViewType {}

extension UICollectionView {
    
    func registerReusableCell<T: UICollectionViewCell>(for cellType: T.Type) {
        self.register(T.self, forCellWithReuseIdentifier: T.reuseIdentifier)
    }
    
    func registerReusableNibCell<Cell: UICollectionViewCell>(for: Cell.Type) {
        let nib = UINib(nibName: Cell.reuseIdentifier, bundle: Bundle(for: Cell.self))
        self.register(nib, forCellWithReuseIdentifier: Cell.reuseIdentifier)
    }
    
    func dequeueReusableCell<T: UICollectionViewCell>(for indexPath: IndexPath) -> T {
        return self.dequeueReusableCell(withReuseIdentifier: T.reuseIdentifier, for: indexPath) as! T
    }
    
}
