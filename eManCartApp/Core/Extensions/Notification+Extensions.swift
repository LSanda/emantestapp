//
//  Notification+Extensions.swift
//  eManCartApp
//
//  Created by Lukáš Šanda on 22.09.18.
//  Copyright © 2018 Lukáš Šanda. All rights reserved.
//

import Foundation

extension NSNotification.Name {
    static let paySummary = NSNotification.Name("paySummary")
    static let showNoConnectionView = NSNotification.Name("showNoConnectionView")
    static let hideNoConnectionView = NSNotification.Name("hideNoConnectionView")
}
