//
//  Strings+Extensions.swift
//  eManCartApp
//
//  Created by Lukáš Šanda on 23.09.18.
//  Copyright © 2018 Lukáš Šanda. All rights reserved.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "\(self)", comment: "")
    }
}
