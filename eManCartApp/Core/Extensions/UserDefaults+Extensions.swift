//
//  UserDefaults+Extensions.swift
//  eManCartApp
//
//  Created by Lukáš Šanda on 20.09.18.
//  Copyright © 2018 Lukáš Šanda. All rights reserved.
//

import Foundation

extension UserDefaults {
    
    struct Keys {
        static let wizardWasDisplayed = "eManCartApp.UserDefaults.WizardWasDisplayed"
        static let currencyCurrent = "eManCartApp.Currency.current"
        static let currencies = "eManCartApp.Currency.allAvailable"
    }
    
    var availableCurrencies: [String: Any]? {
        set {
            set(newValue, forKey: Keys.currencies)
        }
        get {
            return dictionary(forKey: Keys.currencies)
        }
    }
    
    var currentCurrency: String {
        set {
            set(newValue, forKey: Keys.currencyCurrent)
        }
        get {
            return string(forKey: Keys.currencyCurrent) ?? DefaultPrice.key
        }
    }
    
    var isWizardDisplayed: Bool {
        set {
            set(newValue, forKey: Keys.wizardWasDisplayed)
        }
        get {
            return bool(forKey: Keys.wizardWasDisplayed)
        }
    }
}
