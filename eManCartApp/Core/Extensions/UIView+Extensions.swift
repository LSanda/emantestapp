//
//  UIVIew+Extensions.swift
//  eManCartApp
//
//  Created by Lukáš Šanda on 23.09.18.
//  Copyright © 2018 Lukáš Šanda. All rights reserved.
//

import UIKit


extension UIView {
    
    func showBlurBackground() {
        let blurEffect = UIBlurEffect(style: .light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        self.addSubview(blurEffectView)
    }
    
    func removeBlurBackground() {
        self.subviews.compactMap { $0 as? UIVisualEffectView }.forEach {
            $0.removeFromSuperview()
        }
    }
    
    func showBlurLoader() {
        let blurEffect = UIBlurEffect(style: .light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds
        blurEffectView.autoresizingMask = [ .flexibleWidth, .flexibleHeight]
        
        let activityIndicator = UIActivityIndicatorView(style: .gray)
        activityIndicator.center = self.center
        activityIndicator.startAnimating()
        
        blurEffectView.contentView.addSubview(activityIndicator)
        
        self.addSubview(blurEffectView)
    }
    
    func removeBlurLoader(){
        self.subviews.compactMap {  $0 as? UIVisualEffectView }.forEach {
            let view = $0 as UIVisualEffectView
            
            UIView.animate(withDuration: 1, animations: {
                view.alpha = 0
            }, completion: { _ in
                view.removeFromSuperview()
            })
        }
    }
}
