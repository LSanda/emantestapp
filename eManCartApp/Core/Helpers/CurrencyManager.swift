//
//  CurrencyManager.swift
//  eManCartApp
//
//  Created by Lukáš Šanda on 19.09.18.
//  Copyright © 2018 Lukáš Šanda. All rights reserved.
//

import Foundation
import Alamofire


class CurrencyManager {
    
    // MARK: - Properties
    
    private let accessKey = "b9095a1b4ab23da505848fa1617bee83"
    let baseURL = "http://apilayer.net/api/"
    
    static let shared: CurrencyManager = { return CurrencyManager() }()
    
    // MARK: Methods
    
    /// Method for settings exchange ratio to the User Defaults
    ///
    /// - Parameters:
    ///   - currency: Required currency
    ///   - completionHandler: Handler
    func exchangeRatio(for currency: String, completionHandler: @escaping (_ error: Error?) -> ()) {
        if !ReachabilityManager.shared.isConnected { return }

        let url = baseURL + "live?access_key=" + accessKey + "&currencies=" + currency
        
        Alamofire.request(url).responseJSON { (urlRequest) in
            switch urlRequest.result {
            case .success:
                if let response = urlRequest.response, response.statusCode == 200 {
                    if let result = urlRequest.result.value as? NSDictionary {
                        if let quotes = result["quotes"] as? NSDictionary {
                            let key = quotes.allKeys.first as! String
                            let value = quotes.allValues.first as! Double
                            
                            UserDefaults.standard.set(value, forKey: "eManCartApp.Currency." + key)
                            completionHandler(nil)
                        }
                    }
                }
            case .failure(let error):
                completionHandler(error)
            }
        }
    }
    
    /// Method which find key of currency
    ///
    /// - Parameter currency: Required currency
    /// - Returns: Key
    func currencyKey(for currency: String) -> String {
        if let currenciesDict = UserDefaults.standard.availableCurrencies as NSDictionary? {
            return currenciesDict.allKeys(for: currency).first as! String
        }
        
        return ""
    }
    
    /// Method which returns titles of available currencies
    ///
    /// - Returns: Array of titles
    func availableSortedCurrencies() -> [String] {
        if let currenciesDict = UserDefaults.standard.availableCurrencies as? [String: String] {
            var currencies = [String]()
            
            for value in currenciesDict.values {
                currencies.append(value)
            }
            
            return currencies.sorted { $0 < $1 }
        }

        return []
    }
    
    /// Method which get available currencies from the internet via API
    ///
    /// - Parameter completionHandler: Handler
    func getCurrencies(completionHandler: @escaping (_ responseObject: [String: String]?, _ error: Error?) -> ()) {
        if !ReachabilityManager.shared.isConnected { return }
        
        let url = baseURL + "list?access_key=" + accessKey

        Alamofire.request(url).responseJSON { (urlRequest) in
            switch urlRequest.result {
            case .success:
                if let response = urlRequest.response, response.statusCode == 200 {
                    if let result = urlRequest.result.value as? NSDictionary {
                        if let currencies = result["currencies"] as? [String: String] {
                            UserDefaults.standard.availableCurrencies = currencies
                            completionHandler(currencies, nil)
                        }
                    }
                }
            case .failure(let error):
                completionHandler(nil, error)
            }
        }
    }
}

struct DefaultPrice {
    // Default currency`
    static let key = "USD"
    static let price = "United States Dollar"
    
    // Default prices for items
    static let peas = 0.95
    static let eggs = 2.1
    static let milk = 1.3
    static let beans = 0.73
}
