//
//  ProductsManager.swift
//  eManCartApp
//
//  Created by Lukáš Šanda on 21.09.18.
//  Copyright © 2018 Lukáš Šanda. All rights reserved.
//

import Foundation
import CoreData


class ProductsManager {
    
    // MARK: - Properties
    
    var products = [Item]()
    var defaultPrices = [Double]()
    
    static let shared: ProductsManager = { return ProductsManager() }()
    
    // MARK: - Methods
    
    /// Method which returns product
    ///
    /// - Parameter product: Title of required product
    /// - Returns: Product
    func getProduct(_ product: String) -> Item? {
        for item in products {
            if item.name == product {
                return item
            }
        }
        return nil
    }
    
    /// Method which change price of new currency for available items
    ///
    /// - Parameters:
    ///   - currency: New currency
    ///   - completionHandler: Handler
    func changeItemsPrice(for currency: String, completionHandler: @escaping (_ error: Error?) -> ()) {
        if !ReachabilityManager.shared.isConnected { return }
        
        CurrencyManager.shared.exchangeRatio(for: currency) { error in
            if let error = error {
                completionHandler(error)
            }
            let ratio = UserDefaults.standard.double(forKey: "eManCartApp.Currency.USD" + currency)
            
            for product in self.products {
                if let defaultPrice = self.defaultPrice(for: product.name) {
                    product.price = defaultPrice * ratio
                }
            }
            completionHandler(nil)
        }
    }
    
    /// Method which returns default price for required product
    ///
    /// - Parameter item: Title of product
    /// - Returns: Default price
    func defaultPrice(for item: String) -> Double? {
        var fileName = "products"
        fileName += Locale.current.languageCode == "cs-CZ" ? "CZ" : "EN"
        
        if let path = Bundle.main.path(forResource: fileName, ofType: "json") {
            
            do {
                let rawData: NSData = try NSData(contentsOfFile: path)
                let data = try JSONSerialization.jsonObject(with: rawData as Data, options: JSONSerialization.ReadingOptions.allowFragments)
                
                if let array = data as? NSArray {
                    for rawJson in array {
                        if let json = rawJson as? NSDictionary, item == json["name"] as! String {
                            return json["price"] as? Double
                        }
                    }
                }
            } catch {
                fatalError("Problem during reading JSON file with products.")
            }
        }
        
        return nil
    }
    
    /// Method which calculate price of all pieces in the cart of given product
    ///
    /// - Parameter product: Title of product
    /// - Returns: Total price
    func totalPriceForAllInCart(for product: String) -> Double {
        var result = 0.0
        if let item = getProduct(product) {
            let count = Double(CartManager.shared.amountOfProduct(for: product))
            result = item.price * count
        }
        
        return result
    }
    
    /// Method which deserialize products from JSON file
    func initProductsForCart() {
        var fileName = "products"
        fileName += Locale.current.languageCode == "cs-CZ" ? "CZ" : "EN"
        
        if let path = Bundle.main.path(forResource: fileName, ofType: "json") {
            
            do {
                let rawData: NSData = try NSData(contentsOfFile: path)
                let data = try JSONSerialization.jsonObject(with: rawData as Data, options: JSONSerialization.ReadingOptions.allowFragments)
                
                if let array = data as? NSArray {
                    for rawJson in array {
                        let product = Item(context: CoreDataManager.shared.viewContext)
                        if let json = rawJson as? NSDictionary {
                            product.name = json["name"] as! String
                            product.price = json["price"] as! Double
                            product.quantity = json["quantity"] as! String
                            product.photo =  json["photo"] as! String
                            
                            defaultPrices.append(json["price"] as! Double)
                            products.append(product)
                        }
                    }
                }
            } catch {
                fatalError("Problem during reading JSON file with products.")
            }
        }
    }
}
