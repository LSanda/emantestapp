//
//  CartManager.swift
//
//  CartManager.swift
//  eManCartApp
//
//  Created by Lukáš Šanda on 21.09.18.
//  Copyright © 2018 Lukáš Šanda. All rights reserved.
//

import CoreData


class CartManager {
    
    // MARK: - Properties
    
    static let shared: CartManager = { return CartManager() }()
    
    // MARK: - Methods
    
    /// Method which returns count of item in the cart
    ///
    /// - Parameter item: Title of required product
    /// - Returns: Count in the cart
    func amountOfProduct(for item: String) -> Int {
        var result = 0
        if let cart = loadCart() {
            
            if let items = cart.items?.allObjects as? [Item] {
                result = items.filter { $0.name == item }.count
            }
        }
        
        return result
    }
    
    /// Method which calculate total price of products in the cart
    ///
    /// - Returns: Total price
    func summaryPriceForCart() -> Double {
        var summ = 0.0
        
        if let cart = loadCart(), let items = cart.items {
            for item in items {
                if let item = item as? Item {
                    if let product = ProductsManager.shared.getProduct(item.name) {
                        summ += product.price
                    }
                }
            }
        }
        
        return summ
    }
    
    /// Method which load cart from local database
    ///
    /// - Returns: Cart
    func loadCart() -> Cart? {
        let context = CoreDataManager.shared.viewContext
        let fetchRequest: NSFetchRequest<Cart> = Cart.fetchRequest()
        
        do {
            let fetchedCart = try context.fetch(fetchRequest)
            return fetchedCart.last
        } catch {
            fatalError("Problem during fetching from Core Data.")
        }
        
        return nil
    }
}
