//
//  SummaryManager.swift
//  eManCartApp
//
//  Created by Lukáš Šanda on 22.09.18.
//  Copyright © 2018 Lukáš Šanda. All rights reserved.
//

import UIKit


class SummaryManager {
    
    // MARK: - Properties
    static let shared: SummaryManager = { return SummaryManager() }()
    
    // MARK: - Methods
    
    
    /// Method for creating alert for Summary
    ///
    /// - Returns: Alert
    func showAlertWithSummary() -> UIAlertController {
        let key = UserDefaults.standard.currentCurrency
        let price = String(format: "%.2f ", CartManager.shared.summaryPriceForCart()) + key
        
        let message = "SUMMARY_MESSAGE".localized + price
        let alert = UIAlertController(title: "SUMMARY_TITLE".localized, message: message, preferredStyle: .alert)
        let payAction = UIAlertAction(title: "SUMMARY_PAY".localized, style: .default) { action in
            self.paySummary()
        }
        alert.addAction(payAction)
        let cancelAtion = UIAlertAction(title: "ALERT_CANCEL".localized, style: .cancel, handler: nil)
        alert.addAction(cancelAtion)
        return alert
    }
    
    /// Method for paying
    func paySummary() {
        CoreDataManager.shared.removeCart()
        NotificationCenter.default.post(name: .paySummary, object: nil)
    }
}
