//
//  ProductCell.swift
//  eManCartApp
//
//  Created by Lukáš Šanda on 20.09.18.
//  Copyright © 2018 Lukáš Šanda. All rights reserved.
//

import UIKit

class ProductCell: UITableViewCell {
    
    // MARK: - Properties
    
    @IBOutlet weak var imageProduct: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelQuantity: UILabel!
    @IBOutlet weak var labelPrice: UILabel!
    @IBOutlet weak var labelAmountInCart: UILabel!
    @IBOutlet weak var labelPriceForAll: UILabel!
    @IBOutlet weak var labelQuantityDesc: UILabel!
    @IBOutlet weak var labelPriceDesc: UILabel!
    @IBOutlet weak var btnMinus: UIButton!
    @IBOutlet weak var btnPlus: UIButton!
    
    // MARK: - Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        appearanceInit()
        localizeStrings()
    }
    
    /// Configuration context for cell
    ///
    /// - Parameter index: index for getting source information
    func configure(index: Int) {
        let item = ProductsManager.shared.products[index]
        
        self.imageProduct.image = UIImage(named: item.photo)
        self.labelTitle.text = item.name
        self.labelQuantity.text = item.quantity
        self.labelPrice.text = String(format: "%.2f", item.price) + " " + UserDefaults.standard.currentCurrency
        self.labelAmountInCart.text = "PRODUCT_CELL_IN_CART".localized + CartManager.shared.amountOfProduct(for: item.name).description
        
        let priceForAll = ProductsManager.shared.totalPriceForAllInCart(for: item.name)
        self.labelPriceForAll.text = "\(String(format: "%.2f", priceForAll)) " + UserDefaults.standard.currentCurrency
        
        self.btnPlus.tag = index
        self.btnMinus.tag = index
        
        self.btnMinus.isEnabled = CartManager.shared.amountOfProduct(for: item.name) != 0
    }
    
    // MARK: - Private Methods
    
    /// Method for setting appearance of cell
    private func appearanceInit() {
        imageProduct.layer.masksToBounds = false
        imageProduct.layer.cornerRadius = imageProduct.frame.height / 2
        imageProduct.clipsToBounds = true
        
        btnMinus.layer.borderColor = UIColor.black.cgColor
        btnMinus.layer.borderWidth = 1
        btnMinus.layer.masksToBounds = false
        btnMinus.layer.cornerRadius = btnMinus.frame.height / 2
        btnMinus.clipsToBounds = true
        
        btnPlus.layer.masksToBounds = false
        btnPlus.layer.cornerRadius = btnMinus.frame.height / 2
        btnPlus.clipsToBounds = true
    }
    
    /// Method for set localized strings
    private func localizeStrings() {
        self.labelQuantityDesc.text = "PRODUCT_CELL_QUANTITY_DESC".localized
        self.labelPriceDesc.text = "PRODUCT_CELL_PRICE_DESC".localized
    }
}
