//
//  Cart+CoreDataProperties.swift
//  eManCartApp
//
//  Created by Lukáš Šanda on 19.09.18.
//  Copyright © 2018 Lukáš Šanda. All rights reserved.
//
//

import Foundation
import CoreData


extension Cart {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Cart> {
        return NSFetchRequest<Cart>(entityName: "Cart")
    }
    
    @NSManaged public var totalPrice: Double
    @NSManaged public var currency: String
    @NSManaged public var items: NSSet?
    
}

// MARK: Generated accessors for items
extension Cart {
    
    @objc(addItemsObject:)
    @NSManaged public func addToItems(_ value: Item)
    
    @objc(removeItemsObject:)
    @NSManaged public func removeFromItems(_ value: Item)
    
    @objc(addItems:)
    @NSManaged public func addToItems(_ values: NSSet)
    
    @objc(removeItems:)
    @NSManaged public func removeFromItems(_ values: NSSet)
    
    
    /// Method for removing product from the cart
    ///
    /// - Parameter named: Title of product
    func removeProductFromCart(_ named: String) {
        if let cart = CartManager.shared.loadCart() {
            if let items = cart.items?.allObjects as? [Item] {
                if let item = items.first(where: { $0.name == named }) {
                    removeFromItems(item)
                    self.totalPrice = totalPrice < item.price ? 0.0 : totalPrice - item.price
                    
                    CoreDataManager.shared.saveContext()
                }
            }
        }
    }
    
    /// Method for putting item to the cart
    ///
    /// - Parameter item: Item
    func addToCart(_ item: Item) {
        let newItem = Item(context: CoreDataManager.shared.viewContext)
        newItem.name = item.name
        newItem.price = item.price
        newItem.quantity = item.quantity
        
        self.addToItems(newItem)
        
        self.totalPrice += item.price
        
        CoreDataManager.shared.saveContext()
    }
}
