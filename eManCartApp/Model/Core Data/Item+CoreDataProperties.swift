//
//  Item+CoreDataProperties.swift
//  eManCartApp
//
//  Created by Lukáš Šanda on 19.09.18.
//  Copyright © 2018 Lukáš Šanda. All rights reserved.
//
//

import Foundation
import CoreData


extension Item {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Item> {
        return NSFetchRequest<Item>(entityName: "Item")
    }

    @NSManaged public var name: String
    @NSManaged public var price: Double
    @NSManaged public var quantity: String
    @NSManaged public var photo: String
    @NSManaged public var cart: Cart?
}
